# Message Board

A React Native mobile app for message board. This app has both **iOS** and **Android** compatibility

## Installation

- [NodeJS Installation](https://nodejs.org/en/download/)
- [CocaPods Installation](https://guides.cocoapods.org/using/getting-started.html)

### iOS Installation

- [Xcode Installation](https://developer.apple.com/xcode/)

### Android Installation

- [Java SE Development Kit (JDK)](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
- [Additional Information and Set up guide](https://facebook.github.io/react-native/docs/getting-started.html)

## Steps to run the app

In project directory, run the following command in the terminal:

    npm install

If pods were updated, you should ensure you're using the correct npm package counter parts. You may use Xcode and open the `appname.xcworkspace` project file in the `ios` folder or you may run the following from the terminal:

    npx react-native run-ios

or

    npx react-native run-android

Which should launch the app in the **iOS** Simulator or **Android** Emulator respectively. If you wish to test the app on your physical iPhone device, you will need to connect your device to the computer and open the project in Xcode. There you may select which device to deploy the app to.

For android ensure you have your mobile phone connected to the computer and there aren't any android emulators running. This project is AndroidX compatible, therefore you should run `npx react-native run-android` to install the dev app on your device.