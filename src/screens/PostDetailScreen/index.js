import React, { useEffect, useCallback } from 'react';
import { FlatList, View, Text, Image, TouchableOpacity, ActivityIndicator, StyleSheet } from 'react-native';
import { connect } from 'react-redux';

import CustomHeader from '../../components/CustomHeader';
import * as PostActions from '../../actions/PostActions';

import * as CONST from '../../utils/Constants';
import scale, { verticalScale } from '../../utils/Scale';

const PostDetailScreen = (props) => {
  const { commentData, isFetchingComments, userNames, navigation, isPostDeleted } = props;
  const { userName, postId, postTitle, postBody } = navigation.state.params;

  useEffect(() => {
    // Fetch Comments
    props.fetchComments(postId);
  }, []);

  // To navigate back after deleting a post
  useEffect(() => {
    if(isPostDeleted){
      navigation.goBack();
    }
  }, [isPostDeleted, commentData]);

  const renderItem = ({item}) => {
    return (
      <View
        style={styles.commentStyle}
      >
        <Text style={styles.commentNameTextStyle}>
          {userNames[item.id] || item.email.split("@")[0]}
        </Text>
        <Text style={styles.commentTextStyle}>
          {item.body}
        </Text>
      </View>
    );
  };

  const keyExtractor = useCallback((item) => item.id);

  return (
    <View style={styles.container}>
      <CustomHeader
        showBackIcon
        onBackIconPress={() => {
          navigation.goBack();
        }}
        numberOfLines={1}
        titleText={postTitle}
      />
      <View style={styles.bodyContainer}>
        <View style={styles.postStyle}>
          <TouchableOpacity
            style={styles.trashIconContainerStyle}
            onPress={() => {
              props.deletePost(postId);
            }}>
            <Image
              style={styles.trashIconStyle}
              source={require('./../../../assets/common/icons/trash.png')}
            />
          </TouchableOpacity>
          <Text style={styles.postTextStyle} numberOfLines={1}>
            {userName}
          </Text>
          <Text style={styles.postBodyTextStyle}>{postBody}</Text>
        </View>
          {isFetchingComments ? (
            <ActivityIndicator size={'large'} />
          ) : commentData && commentData.length ? (
            <View style={styles.commentBodyContainer}>
              <Text style={styles.postBodyTextStyle}>{'Comments :'}</Text>
              <FlatList
                bounces={false}
                showsVerticalScrollIndicator={false}
                extraData={commentData}
                data={commentData}
                keyExtractor={keyExtractor}
                renderItem={(item) => renderItem(item)}
                contentContainerStyle={styles.postContainerStyle}
              />
            </View>
          ) : (
            <View style={styles.commentBodyContainer}>
              <Text style={styles.noDataText}>{'No comments available'}</Text>
            </View>
          )}
      </View>
    </View>
  );
};

const mapStateToProps = (state) => {
  return {
    commentData: state.PostReducer.commentData,
    userNames: state.PostReducer.userNames,
    isFetchingComments: state.PostReducer.isFetchingComments,
    isPostDeleted: state.PostReducer.isPostDeleted
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchComments: (postId) => dispatch(PostActions.fetchComments(postId)),
    deletePost: (postId) => dispatch(PostActions.deletePost(postId)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(PostDetailScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  bodyContainer: {
    flex: 1,
    backgroundColor: CONST.OFF_WHITE_COLOR,
    paddingHorizontal: scale(20)
  },
  commentBodyContainer: {
    flex: 1,
    justifyContent:'center'
  },
  noDataText: {
    fontSize: scale(18),
    textAlign: 'center',
  },
  postStyle: {
    minHeight: verticalScale(70),
    marginVertical: verticalScale(10),
    marginTop: verticalScale(20),
    backgroundColor: CONST.WHITE_COLOR,
    borderRadius: scale(10),
    justifyContent: 'center',
    paddingHorizontal: scale(15)
  },
  postTextStyle: {
    fontSize: scale(16),
    fontWeight: 'bold',
    marginTop: scale(5),
    paddingRight: scale(30)
  },
  commentStyle: {
    minHeight: verticalScale(70),
    marginBottom: verticalScale(5),
    backgroundColor: CONST.WHITE_COLOR,
    paddingHorizontal: scale(5),
    borderRadius: scale(10),
    justifyContent: 'center',
    paddingHorizontal: scale(10)
  },
  commentNameTextStyle: {
    fontSize: scale(16),
    fontWeight: 'bold',
    marginTop:scale(5)
  },
  commentTextStyle: {
    fontSize: scale(14),
    marginTop: scale(5)
  },
  postBodyTextStyle: {
    fontSize: scale(14),
    marginVertical: scale(10)
  },
  trashIconContainerStyle:{
    position: 'absolute',
    right: 0,
    top: 0,
    height: verticalScale(30),
    width: scale(40),
    justifyContent: 'center',
    alignItems: 'center',
  },
  trashIconStyle:{
    resizeMode: 'cover',
  }
});