import React from 'react';
import { TouchableOpacity, Image, View, Text, StyleSheet } from 'react-native';

import scale, { verticalScale } from './../utils/Scale';
import * as CONST from './../utils/Constants';

export default CustomHeader = (props) => {

    const { showBackIcon, onBackIconPress, titleText, numberOfLines } = props;

    return (
        <View style={styles.headerContainer}>
            <View style={styles.backIconContainer}>
                {showBackIcon && (
                    <TouchableOpacity
                        disabled={!onBackIconPress}
                        onPress={() => {
                            onBackIconPress && onBackIconPress();
                        }}>
                        <Image
                            style={styles.backIconStyle}
                            source={require('./../../assets/common/icons/back.png')}
                        />
                    </TouchableOpacity>
                )}
            </View>
            <Text style={styles.headerText} numberOfLines={numberOfLines}>
                {titleText}{' '}
            </Text>
        </View>
    );
};

const styles = StyleSheet.create({
    headerContainer: {
        height: verticalScale(40),
        alignSelf: 'stretch',
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: scale(30),
        backgroundColor: CONST.HEADER_COLOR,
    },
    backIconContainer: {
        position: 'absolute',
        left: 0,
        height: verticalScale(40),
        justifyContent: 'center',
        alignItems: 'center',
        width: scale(30),
    },
    backIconStyle: {
        resizeMode: 'cover',
        tintColor: CONST.GREY,
        height: scale(12),
        width: scale(10),
    },
    headerText: {
        fontSize: scale(20),
        fontWeight: 'bold',
        color: CONST.HEADER_TITLE_COLOR,
    },
});