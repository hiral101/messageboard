import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import PostScreen from '../screens/PostScreen';
import PostDetailScreen from '../screens/PostDetailScreen';

const StackNavigator = createStackNavigator(
	{
		PostScreen: { screen: PostScreen },
		PostDetailScreen: { screen: PostDetailScreen },
	},
	{
		headerMode: 'none',
	},
);
export default createAppContainer(StackNavigator);
